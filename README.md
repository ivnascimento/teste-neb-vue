# vue

### É necessario ter node instalado!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Login
```
url: http://localhost:8080/
login: adm@iury.com
senha: 123456
```