export default {
    name: 'AdmTemplateMixin',
    computed: {
        menus: {
            get() {
                return this.$router.options.routes.filter((e, i, a) => {
                    return e.titulo !== undefined
                })
            }
        },
    }
}
