import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin';
import { validationMixin } from 'vuelidate'
import { required } from 'vuelidate/lib/validators'
import CursoModel from './CursoModel';
import FormCore from '@/core/components/FormCore/FormCore.vue';

export default {
    name: 'CursoForm',
    components: {AdmTemplate, FormCore},
    mixins: [SituacaoEnvioFormMixin, validationMixin],
    data() {
        return {
            modelPadrao: new CursoModel,
            informacoes: false,
            form: {
                id: null,
                nome: '',
            }
        }
    },
    validations: {
        form: {
            nome: {
                required
            }
        }
    },
    mounted() {
        this.getModelPadraoIdStore();
    }
}
