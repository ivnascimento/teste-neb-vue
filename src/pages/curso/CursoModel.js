import Padrao from '@/core/model/Padrao'

export default class CursoModel extends Padrao {

    uri = '/cursos';
    routeName = 'curso';
    storeName = 'cursos';
    botaoTopo = {
        grid: {
            'texto': 'Cadastrar',
            'routeName': this.routeName
        },
        form: {
            'texto': 'Listar', 
            'routeName': this.routeName + 'Grid'
        }
    };
    cabecalho = {
        grid: {
            'titulo': 'Lista de Cursos',
            'preTitulo': 'Todos os Cursos',
            'mostrar': true
        },
        form: {
            'titulo': 'Cadastro de Cursos', 
            'preTitulo': 'Cursos', 
            'mostrar': true
        }
    };
}
