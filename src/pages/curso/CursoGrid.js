import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin'
import GridCore from '@/core/components/GridCore/GridCore.vue'
import CursoModel from './CursoModel'

export default {
  name: 'CursoGrid',
  components: { AdmTemplate, GridCore },
  mixins: [SituacaoEnvioFormMixin],
  data() {
    return {
      buscaItensPersonalizado: { ordenar: { orderBy: 'nome', sortedBy: 'asc' } },
      modelPadrao: new CursoModel,
      colunasGrid: [
        {
          key: 'id',
          label: '#',
          sortable: true
        },
        {
          key: 'nome',
          label: 'Nome',
          sortable: true,
          tipo: 'texto',
          descricao: 'Nome',
          placeholder: 'Nome'
        },
        {
          key: 'acoes',
          label: 'Ações'
        }
      ]
    }
  }
}
