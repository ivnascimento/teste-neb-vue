import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'

export default {
    name: 'home',
    components: {AdmTemplate},
    data() {
        return {
            usuario: null
        }
    },
    created () {
        let usuarioAux = localStorage.getItem('usuario');
        if (usuarioAux) this.usuario = JSON.parse(usuarioAux);
        else this.$router.push('/login')
    },
    mounted() {
        this.$store.commit('setPadrao', {
            'chave': 'cabecalho', 'dados': {
                'preTitulo': 'Controle  seu sistema',
                'titulo': 'Bem vindo à administração do AdmSchool',
                'mostrar': true
            }
        })
    }
}
