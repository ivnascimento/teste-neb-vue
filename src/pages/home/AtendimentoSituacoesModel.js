import Padrao from '@/core/model/Padrao'

export default class AtendimentoSituacoesModel extends Padrao {
    uri = '/atendimento/situacoes';
    storeName = 'atendimentoSituacoes';
    pathMS = 'logistica';

    constructor() {
        return super();
    }
}
