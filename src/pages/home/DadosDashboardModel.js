import Padrao from '@/core/model/Padrao'

export default class DadosDashboardModel extends Padrao {
    uri = '/dados-dashboard';
    storeName = 'dadosDashboard';
    pathMS = 'logistica';

    constructor() {
        return super();
    }
}
