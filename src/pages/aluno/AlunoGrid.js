import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin'
import GridCore from '@/core/components/GridCore/GridCore.vue'
import AlunoModel from './AlunoModel'

export default {
  name: 'AlunoGrid',
  components: { AdmTemplate, GridCore },
  mixins: [SituacaoEnvioFormMixin],
  data() {
    return {
      buscaItensPersonalizado: { ordenar: { orderBy: 'nome', sortedBy: 'asc' } },
      modelPadrao: new AlunoModel,
      colunasGrid: [
        {
          key: 'id',
          label: '#',
          sortable: true
        },
        {
          key: 'nome',
          label: 'Nome',
          sortable: true,
          tipo: 'texto',
          descricao: 'Nome',
          placeholder: 'Nome'
        },
        {
          key: 'cpf',
          label: 'CPF',
          sortable: true,
          tipo: 'cpf',
          descricao: 'CPF',
          placeholder: 'CPF'
        },
        {
          key: 'acoes',
          label: 'Ações'
        }
      ]
    }
  }
}
