import Padrao from '@/core/model/Padrao'

export default class AlunoModel extends Padrao {

    uri = '/alunos';
    routeName = 'aluno';
    storeName = 'alunos';
    botaoTopo = {
        grid: {
            'texto': 'Cadastrar',
            'routeName': this.routeName
        },
        form: {
            'texto': 'Listar', 
            'routeName': this.routeName + 'Grid'
        }
    };
    cabecalho = {
        grid: {
            'titulo': 'Lista de Alunos',
            'preTitulo': 'Todos os Alunos',
            'mostrar': true
        },
        form: {
            'titulo': 'Cadastro de Alunos', 
            'preTitulo': 'Alunos', 
            'mostrar': true
        }
    };
}
