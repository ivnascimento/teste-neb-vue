import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin';
import { validationMixin } from 'vuelidate'
import { required, email, minLength } from 'vuelidate/lib/validators'
import AlunoModel from './AlunoModel';
import FormCore from '@/core/components/FormCore/FormCore.vue';

export default {
    name: 'AlunoForm',
    components: {AdmTemplate, FormCore},
    mixins: [SituacaoEnvioFormMixin, validationMixin],
    data() {
        return {
            modelPadrao: new AlunoModel,
            informacoes: false,
            form: {
                id: null,
                nome: '',
                cpf: '',
                data_nascimento: '',
                email: '',
                telefone: ''
            }
        }
    },
    validations: {
        form: {
            nome: {
                required
            },
            nome: {
                required
            },
            cpf: {
                minLength: minLength(14),
                required
            },
            data_nascimento: {
                required
            },
            email: {
                email,
                required
            },
            telefone: {
                required
            }
        }
    },
    mounted() {
        this.getModelPadraoIdStore();
    }
}
