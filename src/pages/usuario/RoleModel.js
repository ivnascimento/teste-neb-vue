import Padrao from '@/core/model/Padrao'

export default class RoleModel extends Padrao {

    uri = '/roles';
    routeName = 'role';
    storeName = 'roles';

    constructor() {
        return super();
    }


}
