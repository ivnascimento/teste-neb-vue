import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin';
import {validationMixin} from 'vuelidate'
import {required, sameAs, minLength} from 'vuelidate/lib/validators'
import UsuarioModel from './UsuarioModel';
import RoleModel from './RoleModel';
import FormCore from '@/core/components/FormCore/FormCore.vue';
import store from '@/store'
import clone from "clone";

export default {
    name: 'UsuarioForm',
    components: { AdmTemplate, FormCore },
    mixins: [SituacaoEnvioFormMixin, validationMixin],
    data() {
        return {
            modelPadrao: new UsuarioModel,
            modelRole: new RoleModel,
            informacoes: false,

            form: {
                id: null,
                nome: '',
                email: null,
                senha: null,
                confirmarSenha: null,
                situacao: null,
            },
            selecionadoRoles: [],

            buscando: false,
            salvando: false
        }
    },
    validations: {
        form: {
            nome: {},
            email: {},
            senha: {
                minLength: minLength(6)
            },
            confirmarSenha: {
                'Senha não coincide': sameAs('senha')
            },
            situacao: {
                required
            }
        },
        selecionadoRoles: {
            required
        }
    },
    computed: {
        optionsRoles: {
            get (){
                return (store.state.gridDados[this.modelRole.storeName]) ? store.state.gridDados[this.modelRole.storeName].dados.data : []
            }
        },
    },
    mounted() {
        this.modelRole.get();
        this.getModelPadraoIdStore();
    },
    methods: {
        trataForm() {
            // Roles Selecionados
            this.selecionadoRoles = [];
            for (var index in this.form.roles.data) {
                this.selecionadoRoles.push(this.form.roles.data[index].id)
            }
        },

        onSubmit() {
            this.$v.$touch();
            if (this.$v.$invalid) this.notificacao('erro', 'Campos Obrigatorios!');
            else {
                this.salvando = true;
                this.notificacao('enviando');
                
                let objSend = clone(this.form);
                delete objSend.roles;

                if (objSend.senha == '') delete objSend.senha;

                if (this.selecionadoRoles.length > 0) objSend['roles'] = this.selecionadoRoles;
                else objSend.roles = '';

                this.modelPadrao.save(objSend).then(([err, response]) => {
                    this.retornoPadraoFormulario(response);
                    if(!err){
                        // Atualizar localStorage do usuario logado
                        let usuarioLogado = JSON.parse(localStorage.getItem('usuario'));
                        if(response.body.dados.data.foto && usuarioLogado.id == response.body.dados.data.id) {
                            usuarioLogado.foto=response.body.dados.data.foto;
                            localStorage.setItem('usuario',JSON.stringify(usuarioLogado));
                        }
                        this.voltarPadraoForm();
                        this.onIrParaLista()
                    }
                    this.salvando = false;
                });
            }
        }
    }
}
