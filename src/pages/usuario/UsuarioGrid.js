import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin'
import GridCore from '@/core/components/GridCore/GridCore.vue'
import UsuarioModel from './UsuarioModel'

export default {
    name: 'UsuarioGrid',
    components: { AdmTemplate, GridCore },
    mixins: [SituacaoEnvioFormMixin],
    data() {
        return {
            modelPadrao: new UsuarioModel,
            colunasGrid: [
                {
                    key: 'id',
                    label: '#',
                    sortable: true
                },
                {
                    key: 'nome',
                    label: 'Nome',
                    sortable: true,
                    tipo: 'texto',
                    descricao: 'Nome do Usuário',
                    placeholder: 'Nome do Usuário'
                },
                {
                    key: 'email',
                    label: 'E-mail',
                    sortable: true,
                    tipo: 'texto',
                    descricao: 'E-mail do Usuário',
                    placeholder: 'E-mail do Usuário'
                },
                {
                    key: 'roles.name',
                    label: 'Grupo',
                    sortable: false,
                    tipo: 'selecao-unica',
                    opcoes: [
                        { id: null, valor: 'Todos' },
                        { id: 'superuser', valor: 'superuser' }
                    ]
                },
                {
                    key: 'situacao',
                    label: 'Situação',
                    sortable: true,
                    tipo: 'selecao-unica',
                    opcoes: [
                        { id: 'Ativo', valor: 'Ativo' },
                        { id: 'Bloqueado', valor: 'Bloqueado' },
                    ]
                },
                {
                    key: 'acoes',
                    label: 'Ações'
                }
            ]
        }
    },
}
