import Padrao from '@/core/model/Padrao'

export default class UsuarioModel extends Padrao {

    uri = '/usuarios';
    routeName = 'usuario';
    storeName = 'usuarios';
    botaoTopo = {
        grid: {
            'texto': 'Cadastrar',
            'routeName': this.routeName
        },
        form: {
            'texto': 'Listar', 
            'routeName': this.routeName + 'Grid'
        }
    };
    cabecalho = {
        grid: {
            'titulo': 'Lista de Usuários',
            'preTitulo': 'Todos os Usuários',
            'mostrar': true
        },
        form: {
            'titulo': 'Cadastro de Usuários', 
            'preTitulo': 'Usuários', 
            'mostrar': true
        }
    };

}
