import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin';
import { validationMixin } from 'vuelidate'
import { required } from 'vuelidate/lib/validators'
import MatriculaModel from './MatriculaModel';
import AlunoModel from "@/pages/aluno/AlunoModel";
import CursoModel from "@/pages/curso/CursoModel";
import FormCore from '@/core/components/FormCore/FormCore.vue';
import store from '@/store'
import clone from 'clone'

export default {
    name: 'MatriculaForm',
    components: {AdmTemplate, FormCore},
    mixins: [SituacaoEnvioFormMixin, validationMixin],
    data() {
        return {
            modelPadrao: new MatriculaModel,
            modelAluno: new AlunoModel,
            modelCurso: new CursoModel,
            informacoes: false,
            form: {
                id: null,
                data_matricula: null,
                status: null,
                aluno_id: null,
                curso_id: null
            },

            statusAtual: null,
            statusOptions: [
                {id: 'A', valor: 'Ativa'},
                {id: 'B', valor: 'Bloqueada'},
                {id: 'C', valor: 'Cancelada'},
                {id: 'D', valor: 'Desistente'},
                {id: 'F', valor: 'Finalizada'},
                {id: 'T', valor: 'Trancada'}
            ]
        }
    },
    validations: {
        form: {
            data_matricula: {
                required
            },
            status: {
                required
            },
            aluno_id: {
                required
            },
            curso_id: {
                required
            }
        }
    },
    computed: {
        optionsAluno: {
            get (){
                return (store.state.gridDados[this.modelAluno.storeName]) ? store.state.gridDados[this.modelAluno.storeName].dados.data : []
            }
        },
        optionsCurso: {
            get (){
                return (store.state.gridDados[this.modelCurso.storeName]) ? store.state.gridDados[this.modelCurso.storeName].dados.data : []
            }
        }
    },
    mounted() {
        this.modelAluno.get();
        this.modelCurso.get();
        this.getModelPadraoIdStore();
    },
    methods: {
        trataForm() {
            this.statusAtual = clone(this.form.status);

            if (this.statusAtual === 'T') {
                this.statusOptions = this.statusOptions.filter((ele) => {
                    return ele.id === 'T' || ele.id === 'B'
                })
            } else if (this.statusAtual === 'B') {
                this.statusOptions = this.statusOptions.filter((ele) => {
                    return ele.id !== 'F' && ele.id !== 'T'
                })
            }
        }
    }
}
