import Padrao from '@/core/model/Padrao'

export default class MatriculaModel extends Padrao {

    uri = '/matriculas';
    routeName = 'matricula';
    storeName = 'matriculas';
    botaoTopo = {
        grid: {
            'texto': 'Cadastrar',
            'routeName': this.routeName
        },
        form: {
            'texto': 'Listar', 
            'routeName': this.routeName + 'Grid'
        }
    };
    cabecalho = {
        grid: {
            'titulo': 'Lista de Matriculas',
            'preTitulo': 'Todos os Matriculas',
            'mostrar': true
        },
        form: {
            'titulo': 'Cadastro de Matriculas', 
            'preTitulo': 'Matriculas', 
            'mostrar': true
        }
    };
}
