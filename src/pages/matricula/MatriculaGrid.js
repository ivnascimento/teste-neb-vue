import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin'
import GridCore from '@/core/components/GridCore/GridCore.vue'
import MatriculaModel from './MatriculaModel'

export default {
  name: 'MatriculaGrid',
  components: { AdmTemplate, GridCore },
  mixins: [SituacaoEnvioFormMixin],
  data() {
    return {
      modelPadrao: new MatriculaModel,
      colunasGrid: [
        {
          key: 'id',
          label: '#',
          sortable: true
        },
        {
          key: 'alunos.nome',
          label: 'Aluno',
          tipo: 'texto',
          thClass: 'd-none',
          tdClass: 'd-none',
          descricao: 'Aluno',
          placeholder: 'Aluno'
        },
        {
          key: 'alunos.data.nome',
          label: 'Aluno',
        },
        {
          key: 'cursos.nome',
          label: 'Curso',
          tipo: 'texto',
          thClass: 'd-none',
          tdClass: 'd-none',
          descricao: 'Curso',
          placeholder: 'Curso'
        },
        {
          key: 'cursos.data.nome',
          label: 'Curso',
        },
        {
          key: 'data_matricula',
          label: 'Data Matricula',
          sortable: true,
          tipo: 'date',
          descricao: 'Data Matricula',
          placeholder: '01/06/2020'
        },
        {
          key: 'status',
          label: 'Status',
          sortable: true,
          tipo: 'selecao-multipla',
          opcoes: [
              {id: null, valor: 'Todos'},
              {id: 'A', valor: 'Ativa'},
              {id: 'B', valor: 'Bloqueada'},
              {id: 'C', valor: 'Cancelada'},
              {id: 'D', valor: 'Desistente'},
              {id: 'F', valor: 'Finalizada'},
              {id: 'T', valor: 'Trancada'}
          ]
        },
        {
          key: 'acoes',
          label: 'Ações'
        }
      ]
    }
  }
}
