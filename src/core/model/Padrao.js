import superagent from 'superagent';
import superagentCache from 'superagent-cache';
import store from '@/store';
import camelCase from 'camelcase';
import clone from 'clone';

superagentCache(superagent, {
  backgroundRefresh: false,
  verbose: false,
  defaultExpiration: 1
});

export default class Padrao {
  storeName = ''
  uri = ''
  grid = false;
  iniciado = false;
  pathMS = 'principal'
  apiUrl = {}
  data = [];

  baseURL(tipo) {
    this.apiUrl = store.state.paths[tipo]
    return store.state.paths[tipo]
  }

  async iniciar() {

    if (store.getters.getToken) this.token = store.getters.getToken

    if (this.storeName == '') {
      this.storeName = camelCase(this.uri).replace('/', '')
    }

    this.baseURL(this.pathMS);

    this.iniciado = true;

  }

  validarIdParamnsId(params) {
    let retorno = false;
    if (isNaN(params) === false && params !== null) {
      retorno = params;
    }

    if (params !== null)
      if (!isNaN(params.id)) {
        retorno = params.id;
      }
    return retorno;
  }

  async get(params = null, forceUpdate = false, paramsAdd = {}) {

    forceUpdate = true
    if (!this.iniciado)
      await this.iniciar();
    let urlUri = clone(this.uri);

    if (params) {
      for (let propertyName in params) {
        if (urlUri.indexOf('{' + propertyName + '}') >= 0) {
          urlUri = urlUri.replace('{' + propertyName + '}', params[propertyName])
          delete params[propertyName];
        }
      }
    }


    let paramsId = null
    if (this.validarIdParamnsId(params)) {

      if (params.id) urlUri = urlUri + '/' + params.id;
      else urlUri = urlUri + '/' + params;

      if (forceUpdate == false) store.state.atualizado.ultimo = false;

      paramsId = clone(params)

      if (params.id) delete params.id;
      else params = null
    }

    if (params) {

      if (params.buscaGrid) {
        let queryStringTratada = '';
        let juncao = '';

        for (let propertyName in params.buscaGrid) {

          if (queryStringTratada) {
            juncao = ';'
          }
          queryStringTratada = queryStringTratada + juncao + propertyName + ':' + params.buscaGrid[propertyName]
        }
        params.search = queryStringTratada

      }
      delete params.buscaGrid
    }

    try {
      store.state.buscando = true

      let c = await superagent
        .get(this.apiUrl + urlUri)
        .query(params == null ? paramsAdd : params)
        .set('Authorization', 'Bearer ' + this.token)
        .set('accept', 'json')
        .forceUpdate(forceUpdate);

      store.state.buscando = false

      if (c && c.body && c.body.dados) {

        let ultimo = store.state.atualizado.ultimo ? false : true

        if (this.grid && params != null) {
          store.state.buscaParametros = params.search != undefined ? params.search.length : null;
          store.state.atualizado = { page: params.page, ultimo: ultimo, completo: c.body, storeName: this.storeName, forceUpdate: forceUpdate }
        } else {
          if (typeof (params) != 'undefined' || params == null) {
            if (store.state.dados && store.state.dados[this.storeName]) {
              store.state.dados[this.storeName] = [];
              if (store.state.dados[this.storeName][1]) {
                store.state.dados[this.storeName][1] = { completo: c.body, storeName: this.storeName };
              }
            }
          }
        }

        store.state.ultimoRetorno = c;

        if (c.body.dados.meta) {
          let objSend = { forceUpdate: forceUpdate, 'totalResultados': c.body.dados.meta.pagination.total, 'totalPaginas': c.body.dados.meta.pagination.total_pages, dados: c.body.dados };
          store.commit('setGridDados', { chave: this.storeName, dados: objSend });
        }

      } else {
        console.error('url', this.apiUrl + urlUri)
        console.error('nao tava preparado pra esse resultado', c)
      }

      return [0, c]
    } catch (err) {
      return [1, err]
    }

  }

  async save(params = null, termsToReplace = null, paramsExterno = '') {
    if (!this.iniciado)
      this.iniciar()

    let urlFinal = clone(this.uri);
    if (termsToReplace) {

      for (let propertyName in termsToReplace) {
        urlFinal = urlFinal.replace('{' + propertyName + '}', termsToReplace[propertyName])
      }

    }

    if (params != null) {
      for (let propertyName in params) {
        if (params[propertyName] == null) delete params[propertyName]
        if (params[propertyName] == 'null') params[propertyName] = null
      }
    }

    try {
      let response = undefined;

      let idTratado = this.validarIdParamnsId(params);
      if (params != null && params.idd) {
        params.id = params.idd
        delete params.idd
      }

      if (!idTratado) {
        if (this.token) {
          response = await superagent
            .post(this.apiUrl + urlFinal + paramsExterno)
            .send(params)
            .set('Authorization', 'Bearer ' + this.token)
            .set('accept', 'json')
        } else {
          response = await superagent
            .post(this.apiUrl + urlFinal + paramsExterno)
            .send(params)
            .set('accept', 'json')
        }
      } else {
        if (idTratado == 0) {
          idTratado = '';
        }
        response = await superagent
          .put(this.apiUrl + this.uri + '/' + idTratado + paramsExterno)
          .send(params)
          .set('Authorization', 'Bearer ' + this.token)
          .set('accept', 'json')
      }

      return [0, response]
    } catch (err) {
      if (err.response == undefined && err.status == undefined) {
        err.response = [];
        err.response.status = 380;
      }
      return [1, err.response]
    }


  }

  //enviar o formData capturando pelo ID do formulario melhor forma
  async salvarArquivo(formData, id) {
    if (!this.iniciado)
      this.iniciar()

    try {
      let response = undefined;

      if (id != null) {
        formData.append('_method', 'PUT');
      }

      response = await superagent
        .post(id == null ? (this.apiUrl + this.uri) : (this.apiUrl + this.uri + '/' + id))
        .send(formData)
        .set('Authorization', 'Bearer ' + this.token)
        .set('accept', 'json')

      return [0, response]
    } catch (err) {
      if (err.response == undefined && err.status == undefined) {
        err.response = [];
        err.response.status = 380;
      }
      return [1, err.response]
    }

  }

  async deletar(params = null) {
    if (!this.iniciado)
      this.iniciar();

    try {
      let response = undefined;

      if (this.validarIdParamnsId(params)) {
        response = await superagent
          .del(this.apiUrl + this.uri + '/' + params)
          .query(params)
          .set('Authorization', 'Bearer ' + this.token)
          .set('accept', 'json')

        let indice = store.state.gridDados[this.storeName].dados.data.findIndex((elemento) => {

          return params === elemento.id
        });

        store.state.gridDados[this.storeName].dados.data.splice(indice, 1);
      }

      return [0, response]
    } catch (err) {
      if (err.response == undefined) {
        err.response = [];
        err.response.status = 380;
      }
      return [1, err.response]
    }


  }

  async deletarEmMassa(params = null) {
    if (!this.iniciado) this.iniciar();

    try {
      let response = undefined;

      response = await superagent
        .del(`${this.apiUrl}${this.uri}`)
        .send(params)
        .set('Authorization', `Bearer ${this.token}`)
        .set('accept', 'json');

      return [0, response]
    } catch (err) {
      if (err.response == undefined) {
        err.response = [];
        err.response.status = 380;
      }
      return [1, err.response]
    }
  }

}
