import Padrao from '@/core/model/Padrao'

export default class NewPasswordModel extends Padrao {

    uri = '/auth/forgot-password';
}
