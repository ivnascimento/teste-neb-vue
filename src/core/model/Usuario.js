import Padrao from './Padrao'

import superagent from 'superagent';

export default class Usuario extends Padrao {

  uri = '/usuarios'

  routeName = 'usuario';
  storeName = 'usuarios';
  botaoTopo = {
    grid: {
      'texto': 'Cadastrar',
      'routeName': this.routeName
    },
    form: {
      'texto': 'Listar'
      , 'routeName': this.routeName + 'Grid'
    }
  };
  cabecalho = {
    grid: {
      'titulo': 'Lista de Usuários',
      'preTitulo': 'Todos os Usuários',
      'mostrar': true
    },
    form: {
      'titulo': 'Cadastro de Usuários'
      , 'preTitulo': 'Usuários'
      , 'mostrar': true
    }
  };

  async login(params = null) {
    if (!this.iniciado) await super.iniciar()
    try {
      let response = await superagent
        .post(this.apiUrl + '/auth/login')
        .query(params) 
        .set('Authorization', 'Bearer ' + this.token)
        .set('accept', 'json')

      return [0, response]
    } catch (err) {
      if (err.response == undefined) {
        err.response = [];
        err.response.status = 380;
      }
      return [1, err.response]
    }
  }
}
