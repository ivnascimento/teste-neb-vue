export default {
  name: 'LoginTemplate',
  data() {
    return {
      usuario: false
    }
  },
  created() {
    let usuarioAux = localStorage.getItem('usuario')
    if (usuarioAux) {
      this.usuario = JSON.parse(usuarioAux)
      this.$router.push('/')
    }
  },
  methods: {
    sair() {
      localStorage.clear()
      this.usuario = false
    }
  }
}
