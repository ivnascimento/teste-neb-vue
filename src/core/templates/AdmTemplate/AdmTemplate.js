import store from '@/store'
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin';

import IconBase from '@/core/components/Icons/IconBase.vue';
import IconUser from '@/core/components/Icons/IconUser.vue';

import AdmTemplateMixin from "@/pages/mixins/AdmTemplateMixin";
import AdmTemplateMenu from "@/pages/mixins/AdmTemplateMenu.vue";

export default {
  name: 'AdmTemplate',
  components: { IconBase, IconUser, AdmTemplateMenu },
  mixins: [SituacaoEnvioFormMixin, AdmTemplateMixin],
  props: ['buttonPersonalizado', 'textButton', 'funcButton', 'buttonPersonalizado2', 'textButton2', 'funcButton2', 'confButtonCustom', 'funBtnDropdown'],
  data() {
    return {
      usuario: [],
      permissoes: []
    }
  },
  computed: {
    cabecalho: {
      get() {
        return store.state.cabecalho ? store.state.cabecalho : {};
      }
    },
    empresa: {
      get() {
        return store.state.empresa ? store.state.empresa : {};
      }
    },
    iniciaisUsuario: {
      get() {
        if (this.usuario)
          if (this.usuario.nome)
            return this.usuario.nome.substr(0, 2)

      }
    }
  },
  created() {
    this.login()
  },
  methods: {

    login() {
      let usuarioAux = localStorage.getItem('usuario')
      let perfils = localStorage.getItem('perfils')
      if (usuarioAux) {
        this.usuario = JSON.parse(usuarioAux)

        // PERMISSAO MOSTRAR MENU
        this.permissoes = this.usuario.PERMISSAO
        //

        perfils = JSON.parse(perfils)
        this.$store.commit('setUsuario', this.usuario)
        if (perfils) {
          this.$store.commit('setPerfil', perfils)
        }
      } else {

        this.$router.push('/login')
      }
    },

    clickCheckbox(perfil, chave) {
      perfil.habilitado = !perfil.habilitado ? true : false
      console.log(this.$store.getters.getPerfils, 'gettt')
      localStorage.setItem('perfils', JSON.stringify(this.$store.getters.getPerfils))

    },

    sair() {
      localStorage.clear()
      this.usuario = false
      this.$router.push('/login')
    }
  }
}
