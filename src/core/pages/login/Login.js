import LoginTemplate from '@/core/templates/LoginTemplate/LoginTemplate.vue'
import Usuario from '@/core/model/Usuario';
import NewPasswordModel from "@/core/model/NewPasswordModel";
import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin';
import { validationMixin } from 'vuelidate'
import { required } from 'vuelidate/lib/validators'

export default {
  components: { LoginTemplate },
  mixins: [SituacaoEnvioFormMixin, validationMixin],
  data() {
    return {
      modelNewPassword: new NewPasswordModel,
      informacoes: false,

      form: {
        login: '',
        senha: '',
      },

      newpasswordSave: false,
    }
  },
  name: 'Login',
  validations: {
    form: {
      login: {
        required
      },
      senha: {
        required
      }
    }
  },
  beforeMount () {
    if (window.location.toString().indexOf(('/#/')) > -1) {
      window.location = '/?#/login'
    }
  },
  mounted() {
    this.modelUsuario = new Usuario();
  },
  methods: {
    efetuarLogin() {
      this.errosLaravel = []
      this.notificacao('enviando');
      this.$v.$touch()
      if (this.$v.$invalid) this.notificacao('erro', 'Campos Obrigatórios!');
      else {
        this.modelUsuario.login(this.form).then(([err, data]) => {

          if ([500, 400].indexOf(data.status) != -1) {
            this.notificacao('erro', data.body.message, "Não efetuei login");
          }

          if ([401].indexOf(data.status) != -1 || data.body.erro == 1) {
            this.notificacao('erro', "Usuário ou senha errada", "Não efetuei login");
          }

          if ([200].indexOf(data.status) != -1 && data.body.erro == 0) {
            this.notificacao('salvo', "Bem Vindo", "OK");
            console.log('databody', data.body.dados.data);

            localStorage.setItem('usuario', JSON.stringify(data.body.dados.data))
            this.$store.commit('setUsuario', data.body.dados.data)
            this.$router.push('/')
          }
        });
      }
    }
  }
}
