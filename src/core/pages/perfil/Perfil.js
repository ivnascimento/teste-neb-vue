import SituacaoEnvioFormMixin from '@/core/mixin/SituacaoEnvioFormMixin';
import UsuarioModelCore from '@/core/model/Usuario';
import { validationMixin } from 'vuelidate'
import { required, sameAs, minLength } from 'vuelidate/lib/validators'

import IconBase from '@/core/components/Icons/IconBase.vue';
import IconUser from '@/core/components/Icons/IconUser.vue';

export default {
    name: 'Perfil',
    components: { IconBase, IconUser },
    mixins: [SituacaoEnvioFormMixin, validationMixin],
    data() {
        return {
            modelPadrao: new UsuarioModelCore,
            modelLogin: new UsuarioModelCore,
            informacoes: false,
            form: {
                id: undefined,
                nome: '',
                email: '',
                celular: '',
                foto: '',
                senha_antiga: null,
                senha: null,
                confirmarSenha: null,
                senha_por_email: 0,
            },

            usuario: [],
            checked: false,
        }
    },
    validations: {
        form: {
            senha_antiga: {
                required
            },
            senha: {
                required,
                minLength: minLength(6)
            },
            confirmarSenha: {
                'Senha não coincide': sameAs('senha')
            },
        }
    },
    computed: {
        templateAgil() {
            var uri = (this.$store.state.empresa.templatePadrao ? this.$store.state.empresa.templatePadrao : 'AdmTemplate');
            return () => import(`@/core/templates/AdmTemplate/${uri}.vue`);
        },
    },
    created() {
        let usuarioAux = localStorage.getItem('usuario');
        if (usuarioAux) this.usuario = JSON.parse(usuarioAux);
        else this.$router.push('/login');

        if (this.$store.state.empresa.templatePadrao) {
            this.templatePadrao = this.$store.state.empresa.templatePadrao;
            console.log(this.$store.state.empresa.templatePadrao)
        }
    },
    mounted() {
        this.$store.commit('setPadrao', { 'chave': 'cabecalho', 'dados': { 'titulo': '' } })

        this.form.id = this.usuario.id;
        this.form.foto = this.usuario.foto;
        this.form.nome = this.usuario.nome;
        this.form.email = this.usuario.email;
        this.form.celular = this.usuario.celular;
    },
    methods: {
        previewImage (event) {
            var input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = (e) => {
                    this.form.foto = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
        },
        b64paraBlob(dataURI) {
            var byteString = atob(dataURI.split(',')[1]);
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ab], { type: 'image/jpeg' });
        },

        onSubmitSenha() {
            this.notificacao('enviando');
            let formularioDATA = new FormData()

            if (this.checked) {
                this.$v.$touch()
                if (this.$v.$invalid) {
                    this.notificacao('erro');
                } else {
                    let objSend = {
                        login: (this.usuario.email != null && this.usuario.email != '' ? this.usuario.email : this.usuario.celular),
                        senha: this.form.senha_antiga
                    };

                    this.modelLogin = new UsuarioModelCore();
                    this.modelLogin.login(objSend).then(([err, data]) => {
                        if ([200].indexOf(data.status) != -1 && data.body.erro == 0) {
                            formularioDATA.set('senha_antiga', this.form.senha_antiga);
                            formularioDATA.set('senha', this.form.senha);
                            formularioDATA.set('senha_por_email', this.form.senha_por_email);
                            this.onSubmit(formularioDATA)
                        } else this.notificacao('erro', 'Senha Atual Incorreta')
                    });
                }
            } else this.onSubmit(formularioDATA)
        },

        onSubmit(formularioDATA) {
            if (this.form.foto) {
                if (this.form.foto.split(',')[0] == 'data:image/jpeg;base64') formularioDATA.set('foto', this.b64paraBlob(this.form.foto));
            }

            formularioDATA.set('nome', this.form.nome);
            formularioDATA.set('email', this.form.email);
            formularioDATA.set('celular', this.form.celular);

            this.modelPadrao = new UsuarioModelCore();
            this.modelPadrao.salvarArquivo(formularioDATA, this.form.id).then(([err, response]) => {
                this.retornoPadraoFormulario(response);
                this.$v.$reset();
                if (!err && response.body.dados.data) {
                    this.usuario.nome = response.body.dados.data.nome;
                    this.usuario.foto = response.body.dados.data.foto;
                    this.usuario.email = response.body.dados.data.email;
                    this.usuario.celular = response.body.dados.data.celular;
                    localStorage.setItem('usuario', JSON.stringify(this.usuario));
                    this.$router.push('/')
                }
            });
        },

        sair() {
            localStorage.clear();
            this.usuario = false;
            this.$router.push('/login')
        },
    }
}
