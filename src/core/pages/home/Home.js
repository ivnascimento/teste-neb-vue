
import AdmTemplate from '@/core/templates/AdmTemplate/AdmTemplate.vue'

export default {
  name: 'home',
  components: { AdmTemplate },
  mounted() {
    this.$store.commit('setPadrao', { 'chave': 'cabecalho', 'dados': { mostrar: false } })
  },
}
