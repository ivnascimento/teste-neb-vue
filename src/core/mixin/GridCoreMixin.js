import clone from 'clone'
import store from '@/store';

export default {
  data() {
    return {
      timer: null,
      buscaItens: { ordenar: { orderBy: 'id', sortedBy: 'desc', nome: '' }, externo: {} },
      mostrarModalFiltro: false,
      mostrarModalOrder: false,
      filtrosAtivos: 0,
      searchJoin: 'and',
      paginaUltimaSemFiltro: 1,
      grid: {
        mostrarInputBuscaPadrao: false,
        itens: [],
        carregandoTabela: false,
        orderBy: 'id',
        sortedBy: 'desc',
        resultadosNaPagina: 0,
        filtrosGrid: {
          situacao: 'COMPLETO',
          porPagina: 5,
          page: 1,
          limit: 5,
          totalResultados: 0,
          totalPaginas: 0,
        }
      }
    }
  },
  beforeCreate() {
    store.state.buscando = true
  },
  computed: {
    totalResultados: {
      get() {
        return (store.state.gridDados[this.model.storeName]) ? store.state.gridDados[this.model.storeName].totalResultados : 0;
      }
    },
    buscando: {
      get() {
        return (store.state.buscando) ? store.state.buscando : false;
      }
    },
    dadosItens: {
      get() {
        return (store.state.gridDados[this.model.storeName]) ? store.state.gridDados[this.model.storeName].dados.data : [];
      }
    },
    porPagina: {
      get() {
        if (this.model)
          return (store.state.gridDados[this.model.storeName]) ? store.state.gridDados[this.model.storeName].porPagina : 0;
      }
    },
    atualizadoGridPrincipal() {
      return store.state.gridDados[this.model.storeName]
    },
  },
  mounted() {
    let objSend = { 'totalResultados': 0, 'totalPaginas': 0, dados: { data: [] } };
    store.commit('setGridDados', { chave: this.storeName, dados: objSend });

    if (this.$options.name.indexOf("Form") > -1) {
      this.$store.commit('setPadrao', { 'chave': 'botaoTopo', 'dados': this.model.botaoTopo.form })
      this.$store.commit('setPadrao', { 'chave': 'cabecalho', 'dados': this.model.cabecalho.form })
    }

    let dadosBuscaItensCache = this.buscarStorage('store.buscaItens.' + this.model.storeName)
    if (dadosBuscaItensCache) {
      let utilizarCache = true
      if (isNaN(dadosBuscaItensCache.versao))
        utilizarCache = false

      if (!isNaN(dadosBuscaItensCache.versao))
        if (dadosBuscaItensCache.versao != this.buscaItens.versao)
          utilizarCache = false

      if (utilizarCache) {
        this.buscaItens = dadosBuscaItensCache;
      }
    }

    this.buscarGridTimer(300);
  },
  methods: {
    scrollRef() {
      if (store.scrollReferencia != '') {
        let elmnt = document.getElementById(store.scrollReferencia);
        if (elmnt) {
          elmnt.scrollIntoView(false);
        }
        store.scrollReferencia = '';
      }
    }
    , scrollToTop() {
      console.log('toki scrolltop')
      window.scrollTo(0, 0)
    },
    atualizarSort(ctx) {
      this.buscaItens.ordenar.sortedBy = ctx
      this.Grid()
    },
    atualizarSortApp(ctx) {
      this.buscaItens.ordenar.orderBy = ctx.order.orderBy
      this.buscaItens.ordenar.sortedBy = ctx.order.sortedBy
      this.mostrarModalOrder = false
      this.buscarGridTimer(300)
    },
    atualizarCriterioGrid(chave, option) {
      let typeofx = typeof this.buscaItens[chave]
      console.log(typeofx, 'typeofx');
      console.log(chave, 'chave');
      if (['object', 'array'].indexOf(typeofx) >= 0) {
        this.addOrRemove(this.buscaItens[chave], option);
      }

      if (['string'].indexOf(typeofx) >= 0) {

        this.buscaItens[chave] = option;
      }

      this.buscarGridTimer(300)
    },
    addOrRemove(array, value) {
      var index = array.indexOf(value);

      if (index === -1) {
        array.push(value);
      } else {
        array.splice(index, 1);
      }
    },
    salvarStorage(key, obj) {
      localStorage.setItem(key, JSON.stringify(obj));
    },
    buscarStorage(key) {
      try {
        return JSON.parse(localStorage.getItem(key))
      } catch (e) {
        return false
      }
    },
    buscarGridTimer(ms = 300) {
      if (this.timmer) clearTimeout(this.timmer)

      this.timmer = setTimeout(() => {
        this.model.grid = true;
        this.grid.filtrosGrid.page = 1

        this.salvarStorage('store.buscaItens.' + this.model.storeName, this.buscaItens)
        this.buscarGrid()
      }, ms)

    },
    limparFiltros() {
      this.buscaItens.situacaoRepasseApp = []
      this.buscaItens.etapaRepasseApp = []
      this.buscaItens.nome = ''
      this.buscarGridTimer(50)
    },
    async buscarGrid(params = null) {

      this.queryStringTratada = ''
      this.queryStringTratadaSituacao = ''
      let juncao = ''
      let q = '';
      let qi = '';

      if (params != null) {
        Object.keys(params).forEach((key) => {
          this.buscaItens.dataAgendamento = params[key]
        });
      }
      this.filtrosAtivos = 0;
      this.colunasGrid.map(item => {

        if (this.buscaItens[item.key] || this.buscaItens[item.key] === 0) {
          if (this.queryStringTratada) {
            juncao = ';'
          }

          let buscar = clone(this.buscaItens[item.key].toString())
          if ((typeof this.buscaItens[item.key]) == 'object') {
            buscar = this.buscaItens[item.key].join([':'])
            this.filtrosAtivos = this.filtrosAtivos + this.buscaItens[item.key].length
          }
          else {
            if (buscar.toString())
              this.filtrosAtivos++
          }

          if (buscar) {
            if (typeof (item.colunas) == 'object') {
              let valuesKeys = Object.values(item.colunas);
              Object.keys(item.colunas).map((vv, i) => {
                if (this.queryStringTratada) {
                  juncao = ';'
                }
                if (valuesKeys[i] == 'int') {
                  let re = parseInt(buscar);
                  if (!isNaN(re)) {
                    if (q != '') {
                      juncao = ';'
                    }
                    this.queryStringTratada = this.queryStringTratada + juncao + vv + ':' + buscar
                    this.searchJoin = 'or';
                    qi = qi + vv + ':';
                  }
                } else {
                  q = q + vv + ':';
                  this.queryStringTratada = this.queryStringTratada + juncao + vv + ':' + buscar
                }
              })

            } else {
              this.queryStringTratada = this.queryStringTratada + juncao + item.key + ':' + buscar
            }
          }
          this.searchJoin = 'and';
          if (qi != '') {
            this.searchJoin = 'and';
          }

          if (this.buscaItens.ordenar.searchJoin != undefined) {
            this.searchJoin = this.buscaItens.ordenar.searchJoin
          }
        }

      })

      this.grid.carregandoTabela = true;

      let parametro = {
        page: this.grid.filtrosGrid.page,
        porPagina: this.grid.filtrosGrid.porPagina,
        searchJoin: this.searchJoin,
        paginaUltimaSemFiltro: this.paginaUltimaSemFiltro,
        orderBy: this.buscaItens.ordenar.orderBy,
        sortedBy: this.buscaItens.ordenar.sortedBy,
        limit: this.grid.filtrosGrid.porPagina
        // eslint-disable-next-line no-unused-vars
      };

      if (this.queryStringTratada !== '') parametro.search = this.queryStringTratada

      let passarSeachProPai = { search: this.queryStringTratada };

      if (this.buscaItens.externo != undefined && this.buscaItens.externo) {
        Object.keys(this.buscaItens.externo).map((i) => {
          parametro[i] = this.buscaItens.externo[i]
          passarSeachProPai[i] = this.buscaItens.externo[i]
        });
      }

      // limit personalizado
      if (this.buscaItens.limit) parametro.limit = this.buscaItens.limit

      this.$emit('queryStringTratada', passarSeachProPai);

      this.model.get(parametro).then(([err, response]) => {
        if (err != 0) {
          // limpar busca grid
          localStorage.removeItem('store.buscaItens.' + this.model.storeName)
          let msgErro = 'Parametros incorretos, reinicie a busca'
          if (response.response) this.retornoPadraoFormulario(response.response);
        }


        this.grid.resultadosNaPagina = response.body.dados.meta.pagination.count
        this.grid.carregandoTabela = false

        if (typeof (response) == 'undefined') {
          console.log('response.body', response.body);

          this.grid.resultadosNaPagina = response.body.dados.meta.pagination.count
          this.grid.carregandoTabela = false
        }
        return [0, response]
      })
    }
  }
}
