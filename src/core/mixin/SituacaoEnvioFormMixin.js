import clone from 'clone'
import store from '@/store';

export default {
  data() {
    return {
      padraoValoresForm: [],
      situacaoForm: {
        enviando: false,
        erro: false,
        enviado: false,
        situacao: 'aguardando'
      },
    }
  },
  computed: {
    atualizado() {
      return store.state.atualizado
    },
    campoAtivo: {
      get() {
        return this.form.ativo == 1 ? 'Ativo' : 'Desativado';
      }
    },
    idFormulario: {
      get() {
        return (this.form.id ? this.form.id : false)
      }
    },
  },
  watch: {
    atualizado() {
      if (!this.modelPadrao) return

      if (store.state.atualizado.storeName == this.modelPadrao.storeName && store.state.atualizado.ultimo == false
        && this.modelPadrao) {
        this.form = clone(store.state.atualizado.completo.dados.data);
        if (typeof (this.trataForm) !== 'undefined') this.trataForm()
      }
    }
  },
  mounted() {
    if (typeof (this.aoIniciar) !== 'undefined') this.aoIniciar()

    this.copiarForm(this.form, this.padraoValoresForm)

    if (this.$options.name && this.$options.name.indexOf("Grid") > -1) {

      this.$store.commit('setPadrao', { 'chave': 'botaoTopo', 'dados': this.modelPadrao.botaoTopo.grid })
      this.$store.commit('setPadrao', { 'chave': 'cabecalho', 'dados': this.modelPadrao.cabecalho.grid })
    }
    if (this.$options.name && this.$options.name.indexOf("Form") > -1) {
      this.$store.commit('setPadrao', { 'chave': 'botaoTopo', 'dados': this.modelPadrao.botaoTopo.form })
      this.$store.commit('setPadrao', { 'chave': 'cabecalho', 'dados': this.modelPadrao.cabecalho.form })
    }
  },
  methods: {
    voltarPadraoForm(formulario = this.padraoValoresForm, destino = this.form) {
      for (var property in formulario) {
        destino[property] = clone(formulario[property]);
      }
    },

    setSituacaoEnvioForm(acao) {
      this.situacaoForm.situacao = acao;
    },

    onIrParaLista() {
      this.$router.push({ 'name': (this.modelPadrao.routeName + 'Grid') })
    },

    onReset() {
      this.voltarPadraoForm()
      this.$v.$reset()
    },

    copiarForm(formulario, destino) {
      for (var property in formulario) {
        destino[property] = clone(formulario[property]);
      }
    },

    onSubmitPadrao() {
      this.notificacao('enviando');
      this.$v.$touch();
      if (this.$v.$invalid) this.notificacao('erro', 'Campos Obrigátorios!');
      else {
        this.modelPadrao.save(this.form).then(([err, response]) => {
          this.retornoPadraoFormulario(response)
          this.$v.$reset();
          if (!err) {
            this.voltarPadraoForm();
            this.onIrParaLista();
          }
        });
      }
    },

    onDelete() {
      this.notificacao('enviando', 'deletando');
      this.modelPadrao.deletar(this.form.id).then(([err]) => {
        if (!err) {
          this.notificacao('salvo', "Deletado", "Registro apagado com sucesso!");
          this.$router.push({ 'name': (this.modelPadrao.routeName + 'Grid') })
          this.voltarPadraoForm()
        }
      });
    },
    getByIdPadrao() {
      if (this.$route.params.id) {
        this.modelPadrao.get(this.$route.params.id).then(([err, response]) => {
          if (!err) {
            this.form = clone(response.body.dados.data)
            if (typeof (this.trataForm) !== 'undefined') this.trataForm()
          } else this.notificacao('erro');
        });
      }
    },

    getModelPadraoIdStore(id, form2, model, trataform2, gridDados = true) {
      // console.log('gridDados: ', gridDados);
      if (id == null) id = this.$route.params.id;
      if (model == null) model = 'modelPadrao';
      var dados = [];

      if (gridDados) dados = clone(store.state.gridDados[this.modelPadrao.storeName] ? store.state.gridDados[this.modelPadrao.storeName].dados.data : []);
      else dados = clone(store.state.dados[this.modelPadrao.storeName] ? store.state.dados[this.modelPadrao.storeName].dados : []);

      if (id) {
        if (dados.length > 0) var form = dados.filter((item) => { return item.id == id })[0];

        if (form != null && form != undefined) {
          // console.log('Nao precisei buscar novamente o get! :) ');
          if (form2) this.form2 = form;
          else this.form = form;
          this.buscando = false;
          if (typeof (this.trataForm) !== 'undefined' && !trataform2) this.trataForm();
          if (typeof (this.trataForm2) !== 'undefined' && trataform2) this.trataForm2();
        }
        else {
          this[model].get(id).then(([err, response]) => {
            if (!err) {
              // console.log('Busquei novamente o get id da rota! :( ');
              if (form2) this.form2 = clone(response.body.dados.data);
              else this.form = clone(response.body.dados.data);
              this.buscando = false;
              if (typeof (this.trataForm) !== 'undefined' && !trataform2) this.trataForm();
              if (typeof (this.trataForm2) !== 'undefined' && trataform2) this.trataForm2();
            } else this.notificacao('erro', 'Erro ao buscar no Banco de Dados!');
          });
        }
      }
    },

    retornoPadraoFormulario(response, params = { 'msg': '', 'titulo': '' }) {

      if ([500, 400].indexOf(response.status) != -1) {
        if (response.body != undefined && response.body.dados != undefined && response.body.dados.error != undefined) {
          this.notificacao('erro', response.body.dados.error[0], (response.body && response.body.msg ? response.body.msg : ''));
        } else {

          if (response.body != undefined && response.body.msg && response.body.msg == "Erro Validação" && response.body.dados) {
            Object.keys(response.body.dados).map((i) => {
              if (response.body.dados[i].length > 0) this.notificacao('erro', response.body.dados[i][0])
            });
          } else this.notificacao('erro', (response.body ? response.body.dados : null), (response.body ? response.body.msg : null));

        }
      }
      if ([401].indexOf(response.status) != -1) {
        this.notificacao('erro', 'Sem autenticação', 'Faça login ou peça permissão a este recurso');
      }
      if ([380].indexOf(response.status) != -1) {
        this.notificacao('erro', 'Sem internet', 'Sem conexão 3g ou wifi status: ' + response.status);
      }

      if ([200].indexOf(response.status) != -1) {
        let msg = (response.body.msg) ? response.body.msg : params.msg;
        this.notificacao('salvo', msg, params.titulo);
      }
    },

    notificacao(acao, msg = '', titulo = '', tempo = 3000) {
      this.situacaoForm.situacao = acao;

      switch (acao) {
        case "enviando":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Salvando',
            text: msg ? msg : 'Estamos salvando as informações'
          });
          break;
        case "erro":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Erro',
            text: msg ? msg : 'Necessária validação',
            type: 'error'
          });
          break;
        case "salvo":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Ok Salvo!',
            text: msg ? msg : 'Salvo com sucesso',
            type: 'success'
          });
          break;
        case "aviso":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Aviso!',
            text: msg ? msg : 'Aguarde',
            type: 'warn'
          });
          break;
        default:
          this.$notify(acao);
          break;
      }

    },
    notificacaoSemTime(acao, msg = '', titulo = '', tempo = -1) {
      this.situacaoForm.situacao = acao;

      switch (acao) {
        case "enviando":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Salvando',
            text: msg ? msg : 'Estamos salvando as informações'
          });
          break;
        case "erro":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Erro',
            text: msg ? msg : 'Necessária validação',
            type: 'error'
          });
          break;
        case "salvo":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Ok Salvo!',
            text: msg ? msg : 'Salvo com sucesso',
            type: 'success'
          });
          break;
        case "aviso":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Aviso!',
            text: msg ? msg : 'Aguarde',
            type: 'warn'
          });
          break;
        case "info":
          this.$notify({
            group: 'padrao',
            duration: tempo,
            title: titulo ? titulo : 'Aviso!',
            text: msg ? msg : 'Aguarde',
            type: 'info'
          });
          break;
        default:
          this.$notify(acao);
          break;
      }
    },
    cleanNotificacao() {
      this.$notify({
        group: 'padrao',
        clean: true
      })
    },

    setTituloPage(titulo) {
      this.$store.commit('setPadrao', { 'chave': 'cabecalho', 'dados': { 'titulo': titulo } });
    }
  }
}
