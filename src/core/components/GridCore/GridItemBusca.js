import datePicker from 'vue-bootstrap-datetimepicker'
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css'
import { required } from 'vuelidate/lib/validators'
import filters from '@/core/filters/vTopFilters';

export default {
  name: "GridItemBusca",
  props: ["travado", "listaItem", "item", "buscaItens"],
  mixins: [filters],
  components: {
    datePicker
  },
  data() {
    return {
      elementoAnterior: null,
      options: {
        format: 'DD/MM/YYYY',
        useCurrent: false,
      },
      referenciaPai: null,
      requestAguardar: false,
      aguardarRequestTempo: 1000,
      requestAguardarValor: ''
    }
  },
  validations: {
    "item.valor": {
      required
    },
  },
  methods: {
    alterarInputTimeout() {
      if (this.item.valor === null || this.item.valor === '') delete this.buscaItens[this.item.key];
      else this.buscaItens[this.item.key] = this.item.valor;

      if (this.item.tipo === 'cpf') {
        if (this.item.valor.length >= 14) {
          this.buscaItens[this.item.key] = this.$options.filters.soNumeros(this.item.valor);
          this.buscarDenovo();
        }
        if (this.item.valor.length === 0) this.buscarDenovo();
      } else this.buscarDenovo()
    },

    alterarInputTimeoutParams() {
      if (this.item.valor === null || this.item.valor === '') delete this.buscaItens.externo[this.item.key];
      else this.buscaItens.externo[this.item.key] = this.item.valor;

      this.buscarDenovo()
    },

    buscarDenovo() {
      setTimeout(() => this.$emit('buscarGrid'), 750);
    },

    alterarSelectTimeout() {
      if (this.requestAguardar > 1) return;
      this.buscaItens[this.item.key] = this.item.valor;
      if (this.requestAguardar == 1)
        setTimeout(() => {
          this.requestAguardar = 0;
          this.$emit('buscarGrid');
        }, 500);
      this.requestAguardar++;
    }

  }
}
