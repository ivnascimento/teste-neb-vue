import GridItemBusca from './GridItemBusca.vue'
import BadgeAtivo from '@/core/components/FormCore/BadgeAtivo.vue'
import GridCoreMixin from '@/core/mixin/GridCoreMixin'
import filters from '@/core/filters/vTopFilters';
import store from '@/store'

export default {
  name: 'GridCore',
  mixins: [GridCoreMixin, filters],
  components: { GridItemBusca, BadgeAtivo },
  props: ['colunasGrid', 'model', 'paramentro2', 'NovoNomeButtonEditar', 'buscaItensPersonalizado', 'ocultarBuscaGeral'],
  computed: {
    situacoesUser: {
      get() {
        let obj = store.state.gridDados.situacoesUsuarios ? store.state.gridDados.situacoesUsuarios.dados.data : [];
        obj = obj.map((ele) => {
          ele.name = `${ele.nome} ${ele.descricao ? ele.descricao : ''}`
          return ele
        })
        return obj
      }
    },
  },
  created() {
    if (this.buscaItensPersonalizado != undefined && this.buscaItensPersonalizado != null) {
      this.buscaItens = this.buscaItensPersonalizado;
      console.log('buscaItensPersonalizado: ', this.buscaItensPersonalizado)
    }
  },
  methods: {
    findSituacaoIdUser(id) {
      return this.situacoesUser.find(ele => ele.id === id) ? this.situacoesUser.find(ele => ele.id === id) : null
    },

    statusValue (id) {
      let value = [
        {id: 'A', valor: 'Ativa'},
        {id: 'B', valor: 'Bloqueada'},
        {id: 'C', valor: 'Cancelada'},
        {id: 'D', valor: 'Desistente'},
        {id: 'F', valor: 'Finalizada'},
        {id: 'T', valor: 'Trancada'}
      ]
      let encontrado = value.find(ele => ele.id === id)
      return encontrado ? encontrado.valor : ''    
    }
  }
}
