export default {
  name: 'FormCore',
  props:['registroId', 'ocultarButton', 'ocultarcard', 'ocultarButtonListar'],
  methods: {
    onSubmitInterno: function() {
      this.$emit('onSubmit')
    },
    onResetInterno: function() {
      this.$emit('onReset')
    },
    onDeleteInterno: function() {
      this.$emit('onDelete')
    },
    onIrListaInterno: function() {
      this.$emit('onIrParaLista')
    }
  }
}
