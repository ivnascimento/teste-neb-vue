import moment from 'moment'

export default {
  filters: {
    agora() {
      return moment().format('YYYY-MM-DD HH:mm:ss');
    },

    agoraDate() {
      return moment().format('YYYY-MM-DD');
    },

    agoraHora() {
      return moment().format('HH:mm');
    },

    agoraHoraSemMin() {
      return moment().format('HH');
    },

    agoraMes() {
      return moment().format('MM');
    },

    mesPassado() {
      return moment().subtract(1, 'month').format('MM');
    },

    mesPassadoComAno() {
      return moment().subtract(1, 'month').format('YYYY-MM');
    },

    removeMesInicioDate(meses) {
      return moment().subtract(meses, 'month').startOf('month').format('YYYY-MM-DD');
    },

    removeMesFimDate(meses) {
      return moment().subtract(meses, 'month').endOf('month').format('YYYY-MM-DD');
    },

    mesAtualInicioDate() {
      return moment().startOf('month').format('YYYY-MM-DD');
    },

    mesAtualFimDate() {
      return moment().endOf('month').format('YYYY-MM-DD');
    },

    amanhaDate() {
      return moment().add('days', 1).format('YYYY-MM-DD');
    },

    dataComHora(date) {
      return date === null || date === undefined ? null : moment(date).format('DD/MM/YYYY HH:mm');
    },

    dataComHoraYY(date) {
      return date === null || date === undefined ? null : moment(date).format('DD/MM/YY HH:mm');
    },

    dataComHoraSeg(date) {
      return date === null ? null : moment(date).format('DD/MM/YYYY HH:mm:ss');
    },

    dataComHoraSegEng(date) {
      return date === null ? null : moment(date).format('YYYY-MM-DD HH:mm:ss');
    },

    dataComHoraH(date) {
      return date === null ? null : moment(date).format('DD/MM/YYYY HH:mm').replace(':', 'h');
    },

    data(date) {
      return moment(date).format('DD/MM/YYYY');
    },

    diaDaSemana(date) {
      var weekNumber = moment(date).day();
      if (weekNumber == 0) return 'Domingo';
      if (weekNumber == 1) return 'Segunda Feira';
      if (weekNumber == 2) return 'Terça Feira';
      if (weekNumber == 3) return 'Quarta Feira';
      if (weekNumber == 4) return 'Quinta Feira';
      if (weekNumber == 5) return 'Sexta Feira';
      if (weekNumber == 6) return 'Sábado';
      return '';
    },

    dataEng(date) {
      return moment(date).format('YYYY-MM-DD');
    },

    dataEng2(date) {
      return moment(date, 'DD/MM/YYYY ').format('YYYY-MM-DD');
    },

    dataReduzida(date) {
      return moment(date).format('DD/MM');
    },

    dataReduzidayy(date) {
      return moment(date).format('DD/MM/YY');
    },

    dataReduzidaComHora(date) {
      return moment(date).format('DD/MM H:mm').replace(':', 'h');
    },

    hora(date) {
      return moment(date).format('HH:mm');
    },

    horaComSeg(date) {
      return moment(date).format('HH:mm:ss');
    },

    horaH(date) {
      return moment(date).format('HH:mm').replace(':', 'h');
    },

    dataHumanize(datestart) {
      moment.locale('pt-br')
      var format = 'YYYY/MM/DD HH-mm'
      var start = moment(datestart, format)
      var end = moment()
      var diff = end.diff(start, 'minutes')
      return moment.duration(diff, 'minutes').humanize()
    },

    dateHumanize2paramentros(datestart, dateend) {
      if (datestart == null) {
        return 'Sem Informações'
      }

      moment.locale('pt-br')
      var format = 'YYYY/MM/DD HH-mm'
      var start = moment(datestart, format)
      var end = moment(dateend, format)
      var diff = end.diff(start, 'minutes')

      return moment.duration(diff, 'minutes').humanize()
    },

    datediffDayEng(datestart) {
      var format = 'YYYY-MM-DD HH:mm:ss'
      var start = moment(datestart, format)
      var end = moment()
      return end.diff(start, 'days')
    },

    datediffminEng(datestart) {
      var format = 'YYYY-MM-DD HH:mm:ss'
      var start = moment(datestart, format)
      var end = moment()
      return end.diff(start, 'minutes')
    },

    datediffminEng2datas(datestart, datend) {
      var format = 'YYYY-MM-DD HH:mm:ss'
      var start = moment(datestart, format)
      var end = moment(datend, format)
      return end.diff(start, 'minutes')
    },
    
    horarioCalendario(datestart) {
      moment.locale('pt-br')
      return moment(datestart).calendar();
    },
    mascaraCPF(value) {
      // eslint-disable-next-line
      return value ? value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4") : null;
    },

    soNumeros(value) {
      return value ? value.replace(/[^\d]+/g, '') : null;
    },

    minutos_para_hora(s) {
      var hora = Math.floor(s / 60);

      if (hora > 1) return `${hora} horas`;
      if (hora > 0) return `${hora} hora`;
      else return `${s} min (s)`;
    },

    addMinutosData(date, minutes) {
      return date === null ? null : moment(date).add(minutes, 'minutes').format('YYYY-MM-DD HH:mm:ss');
    },

    addSegundosData(date, seconds) {
      return date === null ? null : moment(date).add(seconds, 'seconds').format('YYYY-MM-DD HH:mm:ss');
    },

    addDiasAgoraBr(dias) {
      return moment().add(dias, 'days').format('DD/MM/YYYY');
    },

    addDiasAgoraEng(dias) {
      return moment().add(dias, 'days').format('YYYY-MM-DD');
    },

    addDiasDateEngForBr(date, dias) {
      return moment(date, "YYYY-MM-DD").add(dias, 'days').format('DD/MM/YYYY');
    },

    addDiasDateEngForEng(date, dias) {
      return moment(date, "YYYY-MM-DD").add(dias, 'days').format('YYYY-MM-DD');
    },

    removeDiasAgoraEng(dias) {
      return moment().subtract(dias, 'd').format('YYYY-MM-DD');
    },

    datediffSegEng(datestart) {
      var format = 'YYYY-MM-DD HH:mm:ss'
      var start = moment(datestart, format)
      var end = moment()
      return end.diff(start, 'seconds')
    },

    datediffSegEng2datas(datestart, datend) {
      var format = 'YYYY-MM-DD HH:mm:ss'
      var start = moment(datestart, format)
      var end = moment(datend, format)
      return end.diff(start, 'seconds')
    },

    dataDescritivo1(date) {
      moment.locale('pt-br');
      return moment(date).format('D MMM');
    },

    dataComHoraHDescritivo(date) {
      moment.locale('pt-br');
      return moment(date).format('HH:mm& DD MMM').replace('&', 'h');
    },

    dataComHoraHDescritivoYYYY(date) {
      moment.locale('pt-br');
      return moment(date).format('HH:mm& DD MMM YYYY').replace('&', 'h');
    },

    dataDescritivoCompleto(date) {
      moment.locale('pt-br');
      return moment(date).format('DD & MMMM & YYYY').replace(/&/g, 'de');
    },

    dataDiaD(date) {
      return moment(date).format('D');
    },

    duas_casas(numero) {
      if (numero <= 9) { numero = `0${numero}` }
      return numero;
    }

  }
}
