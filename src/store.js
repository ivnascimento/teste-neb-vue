import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

let paths = {
  'principal':'http://localhost:81/api/v1'
};

export default new Vuex.Store({
  state: {
    buscando:true,
    buscaParametros:0,
    scrollReferencia: '',
    ultimoHorarioBuscaArr:[],
    atualizado:{ultimo:false},
    paths: paths,
    env: process.env.NODE_ENV,
    empresa: {
      logo: 'img/icone-ead.png',
      nome: 'AdmSchool',
      descricaoLogin:' Bem vindo ao gerenciador do AdmSchool.',
      descricaoRodape: null,
    } ,
    botaoTopo:{routeName:'home',texto:''},
    cabecalho:{titulo:'',preTitulo:''},
    usuarios:[],
    gridDados: {
      roles:{dados:{data:[]},qtdTotal:0,totalResultados:0},
      usuarios:{dados:{data:[]},qtdTotal:0,totalResultados:0},
      cursos:{dados:{data:[]},qtdTotal:0,totalResultados:0},
      alunos:{dados:{data:[]},qtdTotal:0,totalResultados:0},
      matriculas:{dados:{data:[]},qtdTotal:0,totalResultados:0},
    },
    usuario: [],
    perfil: null,
},
getters: {
  getToken: state => {
    return state.usuario.token
  },
  getUsuario: state => {
    return state.usuario
  },
  getCoresRelatorio: state => {
    return state.coresRelatorio
  },
  getPerfils: (state) => {
    return state.perfils
  },
  validaPerfils: state => {
    return state.perfils
  }
},
mutations: {
  setUsuario(state, n) {
    state.usuario = n
  },
  setPerfil(state, n) {
    state.perfils = n
  },
  setTitulo(state, n) {
    state.titulo = n
  },
  setPadrao(state, n) {
    state[n.chave] = n.dados
  },
  setGridDados(state, n) {
    state.gridDados[n.chave] = n.dados
  }
}
})
