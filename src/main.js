import Vue from 'vue'
import App from './core/App.vue'
import store from './store'
import './registerServiceWorker'

import router from './router'

Vue.config.productionTip = false;

window.app2 = new Vue({
    router,
    store,
    render: h => h(App)
});

window.app2.$mount('#app');
