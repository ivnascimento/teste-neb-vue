import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue';
import Notifications from 'vue-notification';
import moment from 'moment';
import Vuelidate from 'vuelidate'
import vuelidateErrorExtractor from "vuelidate-error-extractor";
import ErroTemplate from "@/core/components/erroTemplate";
import VueTheMask from 'vue-the-mask'

import {library} from '@fortawesome/fontawesome-svg-core'
import {
  faCoffee,
  faArrowLeft,
  faDotCircle,
  faMapMarkerAlt,
  faBell,
  faSearch,
  faSortAmountDown,
  faSortAmountUp,
  faCalendarAlt,
  faBars,
  faSignOutAlt,
  faPhone,
  faEnvelopeSquare,
  faComments,
  faAngleLeft,
  faAd,
  faPlusCircle,
  faPaw,
  faCircle,
  faDog,
  faCat,
  faChevronRight,
  faVenus,
  faMars,
  faMinus,
  faPlus,
  faExclamationTriangle,
  faQuestion,
  faCrow,
  faDove,
  faDragon,
  faFeather,
  faFeatherAlt,
  faFish,
  faFrog,
  faHippo,
  faHorse,
  faHorseHead,
  faKiwiBird,
  faOtter,
  faSpider,
  faSyringe,
  faPills,
  faStethoscope,
  faImage,
  faTasks,
  faClock,
  faThumbsUp,
  faCommentAlt,
  faShare,
  faTimes,
  faEdit,
  faPhotoVideo,
  faEllipsisV,
  faShareSquare,
  faCamera,
  faVideo,
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'


library.add(
    faCoffee,
    faShareSquare,
    faArrowLeft,
    faDotCircle,
    faMapMarkerAlt,
    faBell,
    faSearch,
    faSortAmountDown,
    faSortAmountUp,
    faCalendarAlt,
    faBars,
    faSignOutAlt,
    faPhone,
    faEnvelopeSquare,
    faComments,
    faAngleLeft,
    faAd,
    faPlusCircle,
    faPaw,
    faCircle,
    faDog,
    faCat,
    faChevronRight,
    faVenus,
    faMars,
    faMinus,
    faPlus,
    faExclamationTriangle,
    faQuestion,
    faCrow,
    faDove,
    faDragon,
    faFeather,
    faFeatherAlt,
    faFish,
    faFrog,
    faHippo,
    faHorse,
    faHorseHead,
    faKiwiBird,
    faOtter,
    faSpider,
    faSyringe,
    faPills,
    faStethoscope,
    faImage,
    faTasks,
    faClock,
    faThumbsUp,
    faCommentAlt,
    faShare,
    faTimes,
    faEdit,
    faPhotoVideo,
    faEllipsisV,
    faCamera,
    faVideo,
);

Vue.component('font-awesome-icon', FontAwesomeIcon);
require('moment/locale/pt-br');

Vue.use(vuelidateErrorExtractor, {
  template: ErroTemplate,
  messages: {
    required: "O {attribute} é obrigatório",
    minLength: "O {attribute} é obrigatório ter no minimo {min} caracteres",
    email: "Informe um e-mail válido",

  },
  attributes: {
    email: "Email",
    nome: "Nome",
    telefone: "Telefone",
  }
});

Vue.use(require('vue-moment'), {moment});
Vue.use(Notifications);
Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(VueTheMask);
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/pages/home/Home.vue')
    },

    {
      path: '/perfil',
      name: 'perfil',
      component: () => import('@/core/pages/perfil/Perfil.vue')
    },

    {
      path: '/login',
      name: 'login',
      component: () => import('@/core/pages/login/Login.vue')
    },

    // USUARIO
    {
      path: '/usuario',
      name: 'usuario',
      component: () => import('./pages/usuario/UsuarioForm.vue')
    },
    {
      path: '/usuarios',
      name: 'usuarioGrid',
      titulo: 'Usuários',
      component: () => import('./pages/usuario/UsuarioGrid.vue')
    },
    {
      path: '/usuario/:id',
      name: 'usuarioEditar',
      component: () => import('./pages/usuario/UsuarioForm.vue')
    },

    // CURSO
    {
      path: '/curso',
      name: 'curso',
      component: () => import('./pages/curso/CursoForm.vue')
    },
    {
      path: '/cursos',
      name: 'cursoGrid',
      titulo: 'Cursos',
      component: () => import('./pages/curso/CursoGrid.vue')
    },
    {
      path: '/curso/:id',
      name: 'cursoEditar',
      component: () => import('./pages/curso/CursoForm.vue')
    },

    // ALUNO
    {
      path: '/aluno',
      name: 'aluno',
      component: () => import('./pages/aluno/AlunoForm.vue')
    },
    {
      path: '/alunos',
      name: 'alunoGrid',
      titulo: 'Alunos',
      component: () => import('./pages/aluno/AlunoGrid.vue')
    },
    {
      path: '/aluno/:id',
      name: 'alunoEditar',
      component: () => import('./pages/aluno/AlunoForm.vue')
    },

    // MATRICULA
    {
      path: '/matricula',
      name: 'matricula',
      component: () => import('./pages/matricula/MatriculaForm.vue')
    },
    {
      path: '/matriculas',
      name: 'matriculaGrid',
      titulo: 'Matriculas',
      component: () => import('./pages/matricula/MatriculaGrid.vue')
    },
    {
      path: '/matricula/:id',
      name: 'matriculaEditar',
      component: () => import('./pages/matricula/MatriculaForm.vue')
    }

  ]
})
